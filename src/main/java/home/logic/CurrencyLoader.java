package home.logic;

import home.currencies.CurrencyEUR;
import home.currencies.CurrencyGBP;
import home.currencies.CurrencyUSD;
import home.currencies.ListCurrencies;
import home.resources.Messages;

import java.io.EOFException;
import java.io.IOException;
import java.util.TimerTask;

public class CurrencyLoader extends TimerTask {
    @Override
    public void run() {
        // get from file list of currencies with method load()
        try {
            try {
                System.out.println("-------------------------------------------------");
                ListCurrencies listCurrencies = ListCurrencies.load();
                System.out.println(listCurrencies.getAllCurrencies());
                System.out.println("-------------------------------------------------");
                System.out.println(Messages.ENTERDATA);
            } catch (NullPointerException e) {
                CurrencyUSD usd = CurrencyUSD.getCurrencyUSD();
                usd.setCount(0);
                usd.setCoefficient(Messages.USDCOURCE);
                CurrencyEUR eur = CurrencyEUR.getCurrencyEUR();
                eur.setCount(0);
                eur.setCoefficient(Messages.EURCOURCE);
                CurrencyGBP gbp = CurrencyGBP.getCurrencyGBP();
                gbp.setCount(0);
                gbp.setCoefficient(Messages.GPBCOURCE);
                ListCurrencies.getListCurrencies().addCurrency(usd);
                ListCurrencies.getListCurrencies().addCurrency(eur);
                ListCurrencies.getListCurrencies().addCurrency(gbp);
                ListCurrencies.save();
            }
        } catch (EOFException e) {
            CurrencyUSD usd = CurrencyUSD.getCurrencyUSD();
            usd.setCount(0);
            usd.setCoefficient(Messages.USDCOURCE);
            CurrencyEUR eur = CurrencyEUR.getCurrencyEUR();
            eur.setCount(0);
            eur.setCoefficient(Messages.EURCOURCE);
            CurrencyGBP gbp = CurrencyGBP.getCurrencyGBP();
            gbp.setCount(0);
            gbp.setCoefficient(Messages.GPBCOURCE);
            ListCurrencies.getListCurrencies().addCurrency(usd);
            ListCurrencies.getListCurrencies().addCurrency(eur);
            ListCurrencies.getListCurrencies().addCurrency(gbp);
            try {
                ListCurrencies.save();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
