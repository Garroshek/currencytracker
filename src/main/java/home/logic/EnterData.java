package home.logic;

import home.currencies.*;
import home.resources.Messages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class EnterData implements Messages, Runnable {
    public static void enterData() throws IOException {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()) {
            String string = scanner.nextLine();
            string = string.toUpperCase();
            if (string.equals(Messages.EXITAPP)){
                ListCurrencies.save();
                System.exit(0);
            }
            String[] tokens = string.split(" ");
            if (tokens.length != 2) {
                System.out.println(Messages.WRONGDATA);
            } else {
                ListCurrencies listCurrencies = ListCurrencies.getListCurrencies();
                try {
                    Scenarios scenarios = Scenarios.valueOf(tokens[0]);
                    switch (scenarios) {
                        case USD:
                            ArrayList<Currency> collectUSD = listCurrencies.getCollect();
                            CurrencyUSD currencyUSD = CurrencyUSD.getCurrencyUSD();
                            int USDcount = 0;
                            for (Currency c : collectUSD){
                                if (c instanceof CurrencyUSD){
                                    USDcount = c.getCount();
                                }
                            }
                            if (USDcount + Integer.parseInt(tokens[1]) <= 0){
                                currencyUSD.setCount(0);
                            } else {
                                currencyUSD.setCount(USDcount + Integer.parseInt(tokens[1]));
                                currencyUSD.setCoefficient(Messages.USDCOURCE);
                            }
                            listCurrencies.addCurrency(currencyUSD);
                            ListCurrencies.save();
                            System.out.println(Messages.SAVEDDATA);
                            System.out.println(Messages.ENTERDATA);
                            break;
                        case EUR:
                            ArrayList<Currency> collectEUR = listCurrencies.getCollect();
                            CurrencyEUR currencyEUR = CurrencyEUR.getCurrencyEUR();
                            int EURcount = 0;
                            for (Currency c : collectEUR){
                                if (c instanceof CurrencyEUR){
                                    EURcount = c.getCount();
                                }
                            }
                            if (EURcount + Integer.parseInt(tokens[1]) <= 0){
                                currencyEUR.setCount(0);
                            } else {
                                currencyEUR.setCount(EURcount + Integer.parseInt(tokens[1]));
                                currencyEUR.setCoefficient(Messages.EURCOURCE);
                            }
                            listCurrencies.addCurrency(currencyEUR);
                            ListCurrencies.save();
                            System.out.println(Messages.SAVEDDATA);
                            System.out.println(Messages.ENTERDATA);
                            break;
                        case GBP:
                            ArrayList<Currency> collectGBP = listCurrencies.getCollect();
                            CurrencyGBP currencyGBP = CurrencyGBP.getCurrencyGBP();
                            int GBPcount = 0;
                            for (Currency c : collectGBP){
                                if (c instanceof CurrencyGBP){
                                    GBPcount = c.getCount();
                                }
                            }
                            if (GBPcount + Integer.parseInt(tokens[1]) <= 0) {
                                currencyGBP.setCount(0);
                            } else {
                                currencyGBP.setCount(GBPcount + Integer.parseInt(tokens[1]));
                                currencyGBP.setCoefficient(Messages.GPBCOURCE);
                            }
                            listCurrencies.addCurrency(currencyGBP);
                            ListCurrencies.save();
                            System.out.println(Messages.SAVEDDATA);
                            System.out.println(Messages.ENTERDATA);
                            break;
                        default:
                            System.out.println(Messages.WRONGDATA);
                    }
                } catch (IllegalArgumentException e){
                    System.out.println(Messages.WRONGDATA);
                }
            }
        }
    }

    public void run() {
        try {
            enterData();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
