package home.resources;

public interface Messages {
    String WRONGDATA = "You entered wrong command";
    String ENTERDATA = "Enter currency and quantity:";
    String SAVEDDATA = "Saved";
    String EXITAPP = "QUIT";
    String GREETING = "Welcome to your currencyTracker";
    float USDCOURCE = 1.0f;
    float EURCOURCE = 1.11f;
    float GPBCOURCE = 1.46f;
    float RUBCOURCE = 0.015f;
}
