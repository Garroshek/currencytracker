package home.currencies;

import java.io.Serializable;

public abstract class Currency implements Serializable{
    private String name = getClass().getName();
    private float coefficient;
    private int count;

    protected Currency(String name){
        this.name = name;
    }

    public float getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(float coefficient) {
        this.coefficient = coefficient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Currency)) return false;

        Currency currency = (Currency) o;

        if (Float.compare(currency.getCoefficient(), getCoefficient()) != 0) return false;
        if (getCount() != currency.getCount()) return false;
        return !(getName() != null ? !getName().equals(currency.getName()) : currency.getName() != null);

    }

    @Override
    public int hashCode() {
        int result = getName() != null ? getName().hashCode() : 0;
        result = 31 * result + (getCoefficient() != +0.0f ? Float.floatToIntBits(getCoefficient()) : 0);
        result = 31 * result + getCount();
        return result;
    }
}
