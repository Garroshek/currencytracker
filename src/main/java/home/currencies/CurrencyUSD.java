package home.currencies;

import java.io.Serializable;

public class CurrencyUSD extends Currency implements Serializable{
    private static CurrencyUSD currencyUSD = null;
    private CurrencyUSD() {
        super("USD");
    }
    public static CurrencyUSD getCurrencyUSD(){
        if (currencyUSD == null)
            currencyUSD = new CurrencyUSD();
        return currencyUSD;
    }
}
