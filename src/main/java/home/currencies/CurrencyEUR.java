package home.currencies;

public class CurrencyEUR extends Currency{
    private static CurrencyEUR currencyEUR = null;
    private CurrencyEUR() {
        super("EUR");
    }
    public static CurrencyEUR getCurrencyEUR(){
        if (currencyEUR == null)
            currencyEUR = new CurrencyEUR();
        return currencyEUR;
    }
}
