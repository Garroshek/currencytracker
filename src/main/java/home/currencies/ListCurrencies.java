package home.currencies;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;

public class ListCurrencies implements Serializable{
    public static final long serialVersionUID = 7888957306655190382L;
    private ArrayList<Currency> collect = new ArrayList<Currency>();
    private static ListCurrencies listCurrencies = null;
    private ListCurrencies(){}
    public static ListCurrencies getListCurrencies(){
        if(listCurrencies == null)
            listCurrencies = new ListCurrencies();
        return listCurrencies;
    }

    public ArrayList<Currency> getCollect() {
        return collect;
    }

    public void addCurrency(Currency currency){
        if (currency instanceof CurrencyUSD){
            Iterator<Currency> iterator = collect.iterator();
            while (iterator.hasNext()){
                Currency curren = iterator.next();
                if(curren instanceof CurrencyUSD){
                    iterator.remove();
                }
            }
            collect.add(currency);
            return;
        }
        if (currency instanceof CurrencyEUR){
            Iterator<Currency> iterator = collect.iterator();
            while (iterator.hasNext()){
                Currency cur = iterator.next();
                if(cur instanceof CurrencyEUR){
                    iterator.remove();
                }
            }
            collect.add(currency);
            return;
        }
        if (currency instanceof CurrencyGBP){
            Iterator<Currency> iterator = collect.iterator();
            while (iterator.hasNext()){
                Currency cur = iterator.next();
                if(cur instanceof CurrencyGBP){
                    iterator.remove();
                }
            }
            collect.add(currency);
            return;
        }
    }

    public String getAllCurrencies(){
        String output = "";
        for (Currency c : collect){
            if(c.getCount() != 0){
                if (c instanceof CurrencyUSD){
                output += c.getName() + ": " + c.getCount() + "\n";
                } else {
                    output += c.getName() + ": " + c.getCount() + "(USD " + c.getCoefficient() * (float) c.getCount()
                            + ")" + "\n";
                }
            }
        }
        return output;
    }

    public static void save() throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream("currencyList.dat");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        try{
            objectOutputStream.writeObject(listCurrencies);
        } catch(Exception ex){
            System.out.println(ex.getMessage());
        } finally {
            objectOutputStream.close();
            fileOutputStream.close();
        }
    }

    public static ListCurrencies load() throws IOException, ClassNotFoundException {
        try {
            FileInputStream fileInputStream = new FileInputStream("currencyList.dat");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            listCurrencies = (ListCurrencies) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
        } catch (FileNotFoundException e){
            File tmp = new File("currencyList.dat");
            tmp.createNewFile();
        }
        return listCurrencies;
    }
}
