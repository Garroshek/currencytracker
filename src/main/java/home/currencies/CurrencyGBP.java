package home.currencies;

public class CurrencyGBP extends Currency{
    private static CurrencyGBP currencyGBP = null;
    private CurrencyGBP(){ super("GBP");}
    public static CurrencyGBP getCurrencyGBP(){
        if (currencyGBP == null)
            currencyGBP = new CurrencyGBP();
        return currencyGBP;
    }
}
