package home;

import home.logic.EnterData;
import home.logic.GettingCurrencies;
import home.logic.GettingEnterData;
import home.resources.Messages;

import java.io.IOException;

public class App
{
    public static void main( String[] args ) throws IOException, ClassNotFoundException {
        System.out.println(Messages.GREETING);
        GettingCurrencies.startGettingCurrencies();
        GettingEnterData.gettingData();
    }
}
